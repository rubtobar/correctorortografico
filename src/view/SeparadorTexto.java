package view;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Ruben
 */
public class SeparadorTexto {

    /**
     * Expresión regular que representa todo lo que no son palabras del alfabeto
     * español
     */
    private static final String SPLIT_REGEX = "[^a-zA-ZÀ-ÿ]";

    public SeparadorTexto() {

    }

    public List<String> separar(String texto) {
        String[] textoSeparado = texto.split(SPLIT_REGEX);
        List<String> listaPalabras = new java.util.ArrayList<>(Arrays.asList(textoSeparado));

        // Eliminamos los espacios en blanco generados por la división
        listaPalabras.removeIf((word) -> {
            return word.equals("");
        });

        return listaPalabras;
    }

}
