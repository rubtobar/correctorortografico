package view;

import controler.Notificar;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.text.BadLocationException;
import model.Dupla;

/**
 *
 * @author Ruben
 */
public class MenuContextual extends JPopupMenu {

    private List<JMenuItem> opciones;
    final List<Dupla> listaSugerencias;

    public MenuContextual(Notificar controlador, Editor parent, Point palabraPos, String palabra) {

        opciones = new ArrayList();

        // Obtener palabras sugeridas del diccionario
        listaSugerencias = controlador.obtenerSugerencias(palabra);

        // Si la palabra ya es correcta finalizamos el constructor
        if (palabraCorrecta(listaSugerencias, palabra)) {
            return;
        }

        // Creamos una lista de componentes para el menu
        listaSugerencias.forEach((sugerencia) -> {
            opciones.add(new JMenuItem(new AbstractAction(sugerencia.toString()) {
                @Override
                public void actionPerformed(ActionEvent e) {

                    try {
                        // Substituimos la palabra del texto
                        parent.cambiarPalabraTexto(palabraPos, sugerencia.getWord(), Color.BLACK);
                    } catch (BadLocationException ex) {
                    }
                    parent.incorrectas--;
                    parent.actualizarInfo();
                }
            }));
        });

        init();
    }

    private void init() {
        opciones.forEach((opcion) -> {
            this.add(opcion);
        });
    }

    /**
     * Devuelve verdadero si la palabra que se pretende corregir es correcta
     *
     * @param lista de sugerencias
     * @param palabra a comprovar
     * @return Verdadero si la palabra a corregir es correcta
     */
    private boolean palabraCorrecta(List<Dupla> listaSugerencias, String palabra) {
        return listaSugerencias.get(0).getWord().toLowerCase().equals(palabra.toLowerCase());
    }

}
