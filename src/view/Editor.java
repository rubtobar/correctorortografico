package view;

import controler.Notificar;
import java.awt.Color;
import java.awt.Point;
import java.util.List;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import model.Palabra;

/**
 *
 * @author Ruben
 */
public class Editor extends javax.swing.JFrame {

    /**
     * Caracter que inicializa el proceso de correción del texto
     */
    private static final String CORRECTION_TRIGGER = " ";

    /**
     * Nombre para el estilo asignado al texto de entrada
     */
    public static final String STYLE_NAME = "Default";

    private final Notificar controlador;
    private final SeparadorTexto separador;
    private List<String> texto;
    private List<String> textoAnt;
    private int palabras;
    public int incorrectas;

    public Editor(Notificar controlador) {
        initComponents();
        this.controlador = controlador;
        this.separador = new SeparadorTexto();
        this.palabras = 0;
        this.incorrectas = 0;
    }

    public void resaltarErroresOrtograficos(Palabra correciones) throws BadLocationException {
        
        if (correciones.getSimilaresCorrectas().get(0).getDistance() == 0) {
            cambiarPalabraTexto(correciones.getPosicion(), correciones.getPalabra(), Color.BLACK);
        } else {
            cambiarPalabraTexto(correciones.getPosicion(), correciones.getPalabra(), Color.RED);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        editorPanel = new javax.swing.JTextPane();
        titulo = new javax.swing.JLabel();
        subtitulo = new javax.swing.JLabel();
        textInfoLabel = new javax.swing.JLabel();
        idiomaLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        editorPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editorPanelMouseClicked(evt);
            }
        });
        editorPanel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                editorPanelKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(editorPanel);

        titulo.setFont(new java.awt.Font("Myanmar Text", 1, 24)); // NOI18N
        titulo.setText("Corrector automático");

        subtitulo.setText("El texto se corrige a medida que se escribe, la corrección se activa al escribir un espacio. Pulsar sobre ella con boton izquierdo para cambiar una palabra.");

        textInfoLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        textInfoLabel.setText("Palabras: 0 / Correctas: 0 / Incorrectas: 0");

        idiomaLabel.setText("Español");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(subtitulo, javax.swing.GroupLayout.DEFAULT_SIZE, 845, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(titulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(idiomaLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(textInfoLabel)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titulo, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(subtitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textInfoLabel)
                    .addComponent(idiomaLabel))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void editorPanelKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_editorPanelKeyTyped

        // Detección de copiado de texto
        if (evt.isControlDown()) {
            textoAnt = texto;
            texto = separador.separar(this.editorPanel.getText());

            // Actualizamos label de información
            palabras = texto.size();
            actualizarInfo();

            controlador.corregirTexto(texto);
            System.out.println("pegar");

        } else if (CORRECTION_TRIGGER.contains(String.valueOf(evt.getKeyChar()))) {
            
            textoAnt = texto;
            texto = separador.separar(this.editorPanel.getText());

            // Actualizamos label de información
            palabras = texto.size();
            actualizarInfo();

            // Obtener posición de la palabra
            int firstPos = this.editorPanel.getText().lastIndexOf(texto.get(texto.size() - 1));
            int lastPos = texto.get(texto.size() - 1).length();
            
            // Si estamos introduciendo conector no volvemos a corregir la ultima palabra
            if (texto.equals(textoAnt)) {
                return;
            }
            // Llamada al controlador para corregir la palabra introducida
            controlador.corregirPalabra(new Palabra(texto.get(texto.size() - 1), new Point(firstPos, lastPos)));
        }


    }//GEN-LAST:event_editorPanelKeyTyped

    private void editorPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editorPanelMouseClicked

        if (evt.getButton() == 3) {
            createContextualMenu(evt);
        }
    }//GEN-LAST:event_editorPanelMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextPane editorPanel;
    private javax.swing.JLabel idiomaLabel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel subtitulo;
    private javax.swing.JLabel textInfoLabel;
    private javax.swing.JLabel titulo;
    // End of variables declaration//GEN-END:variables

    /**
     * Devuelve la posición de la primera letra de la palabra seleccionada y el
     * tamaño de la palabra
     *
     * @param posición de la seleccion
     *
     * @return La primera y letra de la palabra y el tamaño de esta en forma de
     * Point(firstPos, size)
     */
    private Point getSelectedWord(int selectionStart) {

        char[] cadena = this.editorPanel.getText().toCharArray();

        // Principio de linea
        if (selectionStart >= cadena.length) {
            return new Point(-1, -1);
        }

        // No se ha seleccionado una palabra
        if (!Character.isAlphabetic(cadena[selectionStart])) {
            return new Point(-1, -1);
        }

        // Buscar principo de la palabra
        int principio = selectionStart;
        while (principio > 0
                && !(Character.isAlphabetic(cadena[principio])
                && !Character.isAlphabetic(cadena[principio - 1]))) {
            principio--;
        }

        // Buscar final de la palabra
        int fin = selectionStart;
        while (fin < cadena.length - 1
                && !(Character.isAlphabetic(cadena[fin])
                && !Character.isAlphabetic(cadena[fin + 1]))) {
            fin++;
        }

        return new Point(principio, fin - principio + 1);
    }

    /**
     * Crea un menu contextual en la posición en la que se ha clicado (Si esta
     * se encuentra sobre una palabra del texto). A este se le envia la palabra
     * seleccionada para corregir y se ocupa de mostrar las palabras que se
     * podrian utilizar para corregir.
     *
     * @param evt
     */
    private void createContextualMenu(java.awt.event.MouseEvent evt) {
        /**
         * Obtenemos la palabra seleccionada mediante el metodo getselected
         * word. Para saber que palabra se esta clicando, el viewToModel nos da
         * el valor exacto en base a la posición del raton soobre el modelo.
         */
        Point posicionSeleccionada = getSelectedWord(editorPanel.viewToModel(evt.getPoint()));

        // Posición invalida
        if (posicionSeleccionada.x == -1) {
            return;
        }

        String palabraSeleccionada;
        try {
            palabraSeleccionada = this.editorPanel.getText(posicionSeleccionada.x, posicionSeleccionada.y);
        } catch (BadLocationException ex) {
            palabraSeleccionada = "";
        }

        MenuContextual contextualMenu = new MenuContextual(controlador, this, posicionSeleccionada, palabraSeleccionada);
        contextualMenu.show(evt.getComponent(), evt.getX(), evt.getY());
    }

    public void escribirTextoCorregido(List<Boolean> correciones) throws BadLocationException {

        palabras = 0;
        incorrectas = 0;

        StyledDocument doc = this.editorPanel.getStyledDocument();
        Style style = this.editorPanel.addStyle(STYLE_NAME, null);

        String textoActual = editorPanel.getText();

        char[] cadenaEntrada = textoActual.toCharArray();

        StringBuilder stringBuilder = new StringBuilder();

        int palabra = 0;
        this.editorPanel.setText("");
        for (int i = 0; i < cadenaEntrada.length;) {

            if (!Character.isAlphabetic(cadenaEntrada[i])) {
                stringBuilder.append(cadenaEntrada[i]);
                i++;
            } else {

                //nos saltamos todas las letras
                while (i < cadenaEntrada.length && Character.isAlphabetic(cadenaEntrada[i])) {
                    i++;
                }
                // Escribimos la palabra
                palabras++;
                if (!correciones.get(palabra)) {
                    StyleConstants.setForeground(style, Color.RED);
                    incorrectas++;
                    actualizarInfo();
                }
                doc.insertString(doc.getLength(), texto.get(palabra), style);
                StyleConstants.setForeground(style, Color.BLACK);
                palabra++;

            }

            //hemos acabado con los conectores, los escribimos
            doc.insertString(doc.getLength(), stringBuilder.toString(), style);

            // Reinicimaos el constructor de cadenas
            stringBuilder.delete(0, stringBuilder.length());
        }
    }

    public void cambiarPalabraTexto(Point palabraPos, String palabra, Color color) throws BadLocationException {

        // Añadimos una nueva palabra incorrecta
        if (color == Color.RED) {
            incorrectas++;
            actualizarInfo();
        }

        borrarPalabra(palabraPos);

        escribirPalabra(palabraPos, palabra, color);

    }

    private void borrarPalabra(Point palabraPos) {
        // Borramos la palabra
        editorPanel.setSelectionStart(palabraPos.x);
        // Borramos la palabra y el espacio de despues para cambiar el color
        editorPanel.setSelectionEnd(palabraPos.x + palabraPos.y);
        editorPanel.replaceSelection("");
    }

    private synchronized void escribirPalabra(Point palabraPos, String palabra, Color color) throws BadLocationException {

        StyledDocument doc = editorPanel.getStyledDocument();

        Style style = doc.addStyle(STYLE_NAME, null);

        StyleConstants.setForeground(style, color);

        doc.insertString(palabraPos.x, palabra, style);

        // Envimaos el cursor donde estaba (Al final de la frase)
        editorPanel.setCaretPosition(editorPanel.getText().length());
    }

    public void actualizarInfo() {
        this.textInfoLabel.setText("Palabras: " + palabras
                + " / Correctas: " + (palabras - incorrectas)
                + " / Incorrectas: " + incorrectas);
    }

}
