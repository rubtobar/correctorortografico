/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ruben
 */
public class Dupla {

    final String word;
    final int distance;

    public Dupla(String word, int distance) {
        this.word = word;
        this.distance = distance;
    }

    public String getWord() {
        return word;
    }

    public int getDistance() {
        return distance;
    }
    
    

    @Override
    public String toString() {
        return distance + "d - " + word;
    }

}
