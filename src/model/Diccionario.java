package model;

import controler.Notificar;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Ruben
 */
public class Diccionario {

    private static final String PATH_DICCIONARIO = "build\\classes\\recursos\\ES.dic";
    /**
     * Cantidad de sugerencias para corregir una palabra
     */
    private static final int N_PREDICCIONES = 5;

    private List<Dupla> listaResultados;
    private final List<Boolean> correccion;
    private final Notificar controlador;

    public Diccionario(Notificar controlador) {

        this.controlador = controlador;
        listaResultados = new ArrayList<>();
        correccion = new ArrayList<>();
    }

    /**
     * Devuelve una lista de valores booleanos indicando si las lapabras de la
     * lista son correctas (TRUE) o si son incorrectas (FALSE).
     *
     * @param lista
     */
    public void corregir(List<String> lista) {

        correccion.clear();

        lista.forEach((t) -> {
            List<Dupla> predicciones = getPredictions(t);
            if (predicciones.isEmpty()) {
                correccion.add(Boolean.FALSE); // Palabra incorrecta
            } else if (getPredictions(t).get(0).distance == 0) {
                correccion.add(Boolean.TRUE); // Palabra en el diccionario
            } else if (getPredictions(t).get(0).distance != 0) {
                correccion.add(Boolean.FALSE); // Palabra mal escrita
            }
        });

        /*Al acabar de corregir notificamos al controlador que ponga las correcciones*/
        controlador.mostrarTextoCorregido(correccion);
    }

    /**
     * Dada una palabra se ocupa de encontrar si es correcta o incorrecta y de
     * enviarla para ser marcada en pantalla
     * 
     * @param palabra
     */
    public void corregirPalabra(Palabra palabra) {
        
        List<Dupla> parecidas = getPredictions(palabra.getPalabra());
        
        palabra.addSimilaresCorrectas(parecidas);
        
        controlador.mostrarCorrecciones(palabra);
    }

    /**
     * Obtiene una lista N_PREDICCIONES posibles palabras para substituir la
     * palabra que se ha introducido como parametro encontradas en el
     * diccionario.
     *
     * @param palabra a corregir
     * @return lista de N_PREDICCIONES con palabras para substitución.
     */
    public List<Dupla> getPredictions(String palabra) {

        String palabraMinusculas = palabra.toLowerCase();

        // Consultamos las predicciones antes en el buffer
        try (Stream<String> stream = Files.lines(Paths.get(PATH_DICCIONARIO)).parallel()) {

            listaResultados = stream
                    // Obtenemos la distancia de levenstein por palabra
                    .map(word -> {
                        return new Dupla(word, Levensthein.computeLevenshteinDistance(word, palabraMinusculas));
                    })
                    // Ordenamos de menor a mayor distancia
                    .sorted((o1, o2) -> {
                        return Integer.compare(o1.distance, o2.distance);
                    })
                    .collect(Collectors.toList());

        } catch (IOException e) {
            System.err.println("Error I/O");
        }

        // Si no hay ninguna con la distancia apropiada, retornamos una lista vacia
        if (listaResultados.isEmpty()) {
            return listaResultados;
        }

        // Lista de posibles palabras de como maximo 5 (las de menor distancia)
        listaResultados = listaResultados.subList(0, listaResultados.size() > N_PREDICCIONES
                ? N_PREDICCIONES
                : listaResultados.size());

        // Guardamos la lista en un buffer rapido 
        return listaResultados;

    }

}
