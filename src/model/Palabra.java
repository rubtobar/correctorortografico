/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ruben
 */
public class Palabra {
    private String palabra;
    private List<Dupla> similaresCorrectas;
    private Point posicion;

    public Palabra(String palabra, Point posicion) {
        this.palabra = palabra;
        this.similaresCorrectas = new ArrayList();
        this.posicion = posicion;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public List<Dupla> getSimilaresCorrectas() {
        return similaresCorrectas;
    }

    public void addSimilaresCorrectas(List<Dupla> similaresCorrectas) {
        this.similaresCorrectas.clear();
        this.similaresCorrectas.addAll(similaresCorrectas);
    }

    public Point getPosicion() {
        return posicion;
    }

    public void setPosicion(Point posicion) {
        this.posicion = posicion;
    }
    
    
}
