package controler;

import java.util.List;
import model.Dupla;
import model.Palabra;

/**
 *
 * @author Ruben
 */
public interface Notificar {
    
    public abstract void corregirPalabra(Palabra palabra);
    
    public abstract void corregirTexto(List<String> texto);
    
    public abstract void mostrarCorrecciones(Palabra palabra);
    
    public abstract void mostrarTextoCorregido(List<Boolean> correcciones);
    
    public abstract List<Dupla> obtenerSugerencias(String p);
    
    
}
