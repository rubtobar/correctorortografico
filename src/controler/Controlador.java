package controler;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import model.Diccionario;
import model.Dupla;
import model.Palabra;
import view.Editor;

/**
 *
 * @author Ruben
 */
public class Controlador implements Notificar {

    private final Diccionario diccionario;
    private final Editor editor;

    public Controlador() {
        this.diccionario = new Diccionario(this);
        this.editor = new Editor(this);
        editor.setVisible(true);
    }

    @Override
    public void corregirPalabra(Palabra palabra) {
        diccionario.corregirPalabra(palabra);
    }

    @Override
    public void mostrarCorrecciones(Palabra palabra) {
        try {
            editor.resaltarErroresOrtograficos(palabra);
        } catch (BadLocationException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Dupla> obtenerSugerencias(String p) {
        return diccionario.getPredictions(p);
    }

    @Override
    public void corregirTexto(List<String> texto) {
        diccionario.corregir(texto);
    }

    @Override
    public void mostrarTextoCorregido(List<Boolean> correcciones) {
        try {
            editor.escribirTextoCorregido(correcciones);
        } catch (BadLocationException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
